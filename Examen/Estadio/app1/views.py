from django.shortcuts import render
from .models import Player
from .models import Team
from .models import Stadium
# Create your views here.

def listplayers(request):
	return render (request, 'F1.html', {"Players": Player.objects.all()})

def detailplayers(request):
	return render (request, 'F2.html', {"Player": Player.objects.get(id = id)})

def listteams(request):
	return render (request, 'F3.html', {"Teams": Team.objects.all()})

def detailteams(request):
	return render (request, 'F4.html', {"Team": Team.objects.get(id = id)})

def liststadiums(request):
	return render (request, 'F5.html', {"Stadiums": Stadium.objects.all()})

def detailstadium(request):
	return render (request, 'F6.html', {"Stadium": Stadium.objects.get(id = id)})

