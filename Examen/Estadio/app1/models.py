from django.db import models

# Create your models here.

class Team(models.Model):
	name_team = models.CharField(max_length = 24)
	no_jugadores = models.IntegerField()
	coach =  models.CharField(max_length = 24)
	campionship =  models.CharField(max_length = 24)
	season =  models.BooleanField(default = True)
	slug = models.SlugField()

	def _str_(self):
		return self.name_team

class Player(models.Model):
	name_player = models.CharField(max_length = 24)
	surname_player = models.CharField(max_length= 24)
	nickname =  models.CharField(max_length = 24)
	number = models.IntegerField()
	position = models.CharField(max_length =24)
	name_team =  models.CharField(max_length = 24)
	name_team2 = models.CharField(max_length = 24)
	active = models.BooleanField(default =  True)
	slug  = models.SlugField()

	def _str_(self):
		return self.name_player

class Stadium(models.Model):
     name_stadium =  models.CharField(max_length = 24)
     name_team =  models.CharField(max_length = 24)
     capacity = models.IntegerField()
     pet =  models.CharField(max_length = 24)
     slug = models.SlugField()

     def _str_(self):
     	return self.name_stadium