from django.urls import path
from app1 import views

urlpatterns = [

path('F1/', views.listplayers, name = 'listplayers'),
path('F2/<int:id>', views.detailplayers, name = 'detailplayers'),
path('F3/', views.listteams, name = 'listteams'),
path('F4/<int:id>', views.detailteams, name = 'detailteams'),
path('F5/', views.liststadiums, name = 'liststadiums'),
path('F6/<int:id>', views.detailstadium, name = 'detailstadium'),


]

