from django.contrib import admin
from app1.models import Team,Stadium,Player
# Register your models here.

admin.site.register(Team)
admin.site.register(Stadium)
admin.site.register(Player)